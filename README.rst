.. -*- restructuredtext -*-

==============================================================
Memo INFO-F403 *Introduction to language theory and compiling*
==============================================================

**Memo**
about INFO-F403
*Introduction to language theory and compiling*
(course by Gilles Geeraerts, ULB, 2016)

* `PDF document`_
* `Booklet format in PDF`_
* `Booklet format in PostScript`_
* `LaTeX sources`_

.. _`Booklet format in PDF`: https://bitbucket.org/OPiMedia/memo-info-f403-introduction-to-language-theory-and-compiling/raw/master/memo/INFO_F403_Introduction_to_language_theory_and_compiling_2016__memo__booklet.pdf
.. _`Booklet format in PostScript`: https://bitbucket.org/OPiMedia/memo-info-f403-introduction-to-language-theory-and-compiling/raw/master/memo/INFO_F403_Introduction_to_language_theory_and_compiling_2016__memo__booklet.ps
.. _`LaTeX sources`: https://bitbucket.org/OPiMedia/memo-info-f403-introduction-to-language-theory-and-compiling/src/master/memo/
.. _`PDF document`: https://bitbucket.org/OPiMedia/memo-info-f403-introduction-to-language-theory-and-compiling/raw/master/memo/INFO_F403_Introduction_to_language_theory_and_compiling_2016__memo.pdf

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



|Memo|

(picture_ Satya Juan)

.. _picture: http://slideplayer.info/slide/3953093/

.. |Memo| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/12/664771608-1-memo-info-f403-introduction-to-languag_avatar.png
